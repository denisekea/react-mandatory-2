import React, { useContext } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import LoginPage from "./Auth/LoginPage";
import LogoutPage from "./Auth/LogoutPage";
import ListsPage from "./Todos/ListsPage";
import ProfilePage from "./Profile/ProfilePage";
import AuthContext from "../common/AuthContext/AuthContext";

const Routes = () => {
  const { auth } = useContext(AuthContext);
  console.log(auth);

  return (
    <Switch>
      <Route exact path="/">
        {auth ? <Redirect to="/todos" /> : <Redirect to="/login" />}
      </Route>
      <Route
        path="/login"
        component={(props) =>
          auth ? <Redirect to="/todos" /> : <LoginPage {...props} />
        }
      />
      <Route path="/logout" component={(props) => <LogoutPage {...props} />} />
      <Route
        path="/todos"
        component={(props) =>
          auth ? <ListsPage {...props} /> : <Redirect to="/login" />
        }
      />
      <Route
        path="/profile"
        component={(props) =>
          auth ? <ProfilePage {...props} /> : <Redirect to="/login" />
        }
      />
    </Switch>
  );
};

export default Routes;
