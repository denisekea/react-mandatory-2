import React, { useState } from "react";
import styled from "styled-components";

export const FormContainer = styled("div")`
  width: 21%;
  margin: 1em auto;

  form {
    display: grid;
    grid-template-columns: 1fr 5em;
    column-gap: 0.3em;
  }

  input {
    font-family: "Raleway", sans-serif;
    box-sizing: border-box;
    height: 30px;
    border: none;
    border-radius: 5px;
    padding: 2px 5px;
    font-size: 0.9em;

    &:focus {
      outline: none;
      box-shadow: 0 0 0 2px #69f6b7;
    }
  }

  button {
    font-family: "Raleway", sans-serif;
    font-weight: bold;
    height: 30px;
    background-color: #69f6b7;
    border: none;
    border-radius: 5px;
    cursor: pointer;

    &:hover {
      background-color: #a6f9d4;
    }

    &:focus {
      outline: none;
    }
  }
`;

const NewListForm = ({ updateLists }) => {
  const [listName, setListName] = useState("");

  const handleListSubmit = async (e) => {
    e.preventDefault();

    // add new list to data
    const connection = await fetch("/lists/add", {
      method: "POST",
      body: JSON.stringify({ listName }),
      headers: { "Content-Type": "application/json" },
    });
    const data = await connection.json();
    if (data.listData) {
      updateLists(data.listData); // update list overview
      setListName(""); // reset form
    } else {
      console.log(data.error);
    }
  };

  const handleInputChange = (e) => {
    setListName(e.target.value);
  };

  return (
    <FormContainer>
      <form id="newListForm" onSubmit={handleListSubmit}>
        <input
          type="text"
          name="listName"
          value={listName}
          placeholder="Type a new list name..."
          onChange={handleInputChange}
        />
        <button>Add list</button>
      </form>
    </FormContainer>
  );
};

export default NewListForm;
