import React, { useState } from "react";
import styled from "styled-components";
import TodoItem from "./TodoItem";
import NewItemForm from "./NewItemForm";
import ListHeader from "./ListHeader";

const ListContainer = styled("div")`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 2.5em auto auto 2.2em;
  row-gap: 1em;
  background-color: white;
  border-radius: 10px;
  padding: 1em;
  height: auto;
`;

const TodoList = ({ list, updateLists }) => {
  const [listItems, setListItems] = useState(list.listItems);

  const handleUpdateListItems = (listItems) => {
    setListItems(listItems);
  };

  return (
    <ListContainer>
      <ListHeader list={list} updateLists={updateLists} />
      <div>
        {listItems.length
          ? listItems
              .filter((item) => !item.completed)
              .map((item) => {
                return (
                  <TodoItem
                    key={item._id}
                    listId={list._id}
                    item={item}
                    updateListItems={handleUpdateListItems}
                  />
                );
              })
          : "Nothing to do"}
      </div>
      <div style={{ borderTop: "1px solid black", paddingTop: "1em" }}>
        {listItems
          .filter((item) => item.completed)
          .map((item) => {
            return (
              <TodoItem
                key={item._id}
                listId={list._id}
                item={item}
                updateListItems={handleUpdateListItems}
              />
            );
          })}
      </div>
      <NewItemForm listId={list._id} updateListItems={handleUpdateListItems} />
    </ListContainer>
  );
};

export default TodoList;
