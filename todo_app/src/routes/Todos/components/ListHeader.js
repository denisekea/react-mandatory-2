import React from "react";
import styled from "styled-components";
import { FiTrash2 as DeleteIcon } from "react-icons/fi";

const ListHeaderContainer = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1.5em;
  column-gap: 5px;
  align-items: center;

  h2 {
    display: inline-block;
    font-family: "Montserrat", sans-serif;
    margin: 0;
    font-size: 1.4em;
    font-weight: 700;
  }

  svg {
    cursor: pointer;
    stroke: lightgrey;
    height: 17px;
    width: 17px;

    &:hover {
      stroke: red;
    }
  }
`;

const ListHeader = ({ list, updateLists }) => {
  const handleDeleteList = async () => {
    const connection = await fetch(`/lists/${list._id}`, {
      method: "DELETE",
    });
    const data = await connection.json();
    if (data.listData) {
      updateLists(data.listData);
    } else {
      console.log(data.error);
    }
  };

  return (
    <ListHeaderContainer>
      <h2>{list.listName}</h2>
      <DeleteIcon onClick={handleDeleteList} />
    </ListHeaderContainer>
  );
};

export default ListHeader;
