import React, { useState } from "react";
import styled from "styled-components";
import { FiPlus as PlusIcon } from "react-icons/fi";

export const FormContainer = styled("div")`
  width: 100%;
  padding-top: 5px;

  form {
    display: grid;
    grid-template-columns: 1fr 30px;
    column-gap: 0.3em;
  }

  input {
    font-family: "Raleway", sans-serif;
    box-sizing: border-box;
    height: 30px;
    border: none;
    border-radius: 5px;
    padding: 2px 5px;
    font-size: 0.9em;
    background-color: #efeafd;

    &:focus {
      outline: none;
      box-shadow: 0 0 0 2px #69f6b7;
    }
  }

  button {
    font-family: "Raleway", sans-serif;
    font-weight: bold;
    height: 30px;
    width: 30px;
    padding-top: 2px;
    background-color: #69f6b7;
    border: none;
    border-radius: 50%;
    cursor: pointer;

    &:hover {
      background-color: #a6f9d4;
    }

    &:focus {
      outline: none;
    }
  }
`;

const NewItemForm = ({ listId, updateListItems }) => {
  const [newItemName, setNewItemName] = useState("");

  const handleListSubmit = async (e) => {
    e.preventDefault();

    if (newItemName) {
      const connection = await fetch(`/lists/${listId}`, {
        method: "POST",
        body: JSON.stringify({ newItemName }),
        headers: { "Content-Type": "application/json" },
      });
      const data = await connection.json();
      updateListItems(data.listItems); // update list items
      setNewItemName(""); // reset form
    }
  };

  const handleInputChange = (e) => {
    setNewItemName(e.target.value);
  };

  return (
    <FormContainer>
      <form onSubmit={handleListSubmit}>
        <input
          type="text"
          name="newItemName"
          value={newItemName}
          placeholder="Add a new item..."
          onChange={handleInputChange}
        />
        <button>
          <PlusIcon />
        </button>
      </form>
    </FormContainer>
  );
};

export default NewItemForm;
