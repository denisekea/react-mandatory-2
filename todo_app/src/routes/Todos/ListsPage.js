import React, { useState, useEffect } from "react";
import styled from "styled-components";
import NavBar from "../../common/Navigation/NavBar";
import TodoList from "./components/TodoList";
import NewListForm from "./components/NewListForm";

const ListOverviewContainer = styled("div")`
  position: relative;
  height: 100%;
  overflow: auto;

  h1 {
    font-family: "Montserrat", sans-serif;
    text-align: center;
    color: white;
    margin: 1em;
  }
`;

const ListsGrid = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 1em;
  max-width: 80vw;
  margin: 3em auto 5em;

  @media (min-width: 1550px) {
    max-width: 65vw;
  }
`;

const ListsPage = ({ history }) => {
  const [lists, setLists] = useState([]);

  useEffect(() => {
    const fetchLists = async () => {
      const response = await fetch("/lists");
      const data = await response.json();
      setLists(data.listData);
    };
    fetchLists();
  }, []);

  const handleUpdateLists = (newListData) => {
    setLists(newListData);
  };

  return (
    <ListOverviewContainer>
      <NavBar history={history} />
      <h1>SHARED LISTS</h1>
      <NewListForm updateLists={handleUpdateLists} />
      <ListsGrid>
        {lists &&
          lists.map((list) => {
            return (
              <TodoList
                key={list._id}
                list={list}
                updateLists={handleUpdateLists}
              />
            );
          })}
      </ListsGrid>
    </ListOverviewContainer>
  );
};

export default ListsPage;
