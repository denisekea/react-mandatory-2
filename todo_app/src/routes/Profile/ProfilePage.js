import React, { useContext, useState } from "react";
import styled from "styled-components";
import NavBar from "../../common/Navigation/NavBar";
import AuthContext from "../../common/AuthContext/AuthContext";
import SavedConfirmation from "../../common/Notification/SavedConfirmation";

const ProfileContainer = styled("div")`
  position: relative;
  height: 100%;
  overflow: auto;
`;

const TabContainer = styled("div")`
  background-color: white;
  height: 50%;
  width: 50%;
  max-width: 500px;
  border-radius: 10px;
  padding: 2em;
  margin: 8% auto auto;

  h1 {
    font-family: "Montserrat", sans-serif;
    margin: 0;
    margin-bottom: 1em;
    font-size: 2.2em;
    font-weight: 700;
  }

  button {
    height: 30px;
    font-family: "Raleway", sans-serif;
    font-weight: bold;
    font-size: 0.9em;
    background-color: #69f6b7;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    padding: 5px 15px;
    margin-top: 0.5em;

    &:hover {
      background-color: #a6f9d4;
    }
  }
`;

const ProfilePage = ({ history }) => {
  const [notification, setNotification] = useState(false);
  const { auth, handleAuthChange } = useContext(AuthContext);

  const handleDeleteAccount = async () => {
    const { _id } = auth;
    const connection = await fetch(`/auth/${_id}`, {
      method: "DELETE",
    });
    const data = await connection.json();
    if (data.dCount) {
      setNotification(true);
      setTimeout(() => {
        setNotification(false);
        handleAuthChange(false);
      }, 2200);
    } else {
      console.log(data.error);
    }
  };

  return (
    <ProfileContainer>
      <NavBar history={history} />
      <TabContainer>
        <h1>Your profile</h1>
        <h3>Name: {auth.fullName || "Mystery user"}</h3>
        <h3>Email: {auth.email || ""}</h3>
        <button onClick={handleDeleteAccount}>Delete account</button>
      </TabContainer>
      {notification && <SavedConfirmation text={"Deleted your account"} />}
    </ProfileContainer>
  );
};

export default ProfilePage;
