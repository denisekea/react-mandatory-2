import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Redirect } from "react-router-dom";

const LogoutContainer = styled("div")`
  position: relative;
  height: 100%;
  overflow: auto;
  display: flex;
  align-items: center;
  justify-content: center;

  h1 {
    font-family: "Montserrat", sans-serif;
    margin: 0;
    font-size: 3em;
    font-weight: 700;
    color: white;
  }
`;

const LogoutPage = () => {
  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    setTimeout(() => setRedirect(true), 2500);
  }, []);

  return redirect ? (
    <Redirect to="/login" />
  ) : (
    <LogoutContainer>
      <h1>See you next time!</h1>
    </LogoutContainer>
  );
};

export default LogoutPage;
