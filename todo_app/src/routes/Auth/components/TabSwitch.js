import React from "react";
import styled from "styled-components";

const SwitchContainer = styled("div")`
  height: 60px;
  width: 100%;
  border-radius: 10px;
  background-color: #edf0f1;

  button {
    width: 50%;
    height: 100%;
    border: none;
    outline: none;
    cursor: pointer;
    background-color: #edf0f1;
    color: #aeb4ba;
    font-family: "Montserrat", sans-serif;
    font-size: 0.95em;

    &:first-of-type {
      border-radius: 10px 10px 0 0;
    }

    &:nth-of-type(2) {
      border-radius: 10px 10px 0 0;
    }

    &.active {
      background-color: white;
      color: rgba(62, 0, 235, 1);
      font-size: 1.1em;
      /* border-bottom: 3px solid rgba(62, 0, 235, 1); */
    }
  }
`;

const TabSwitch = ({ view, changeView }) => {
  return (
    <SwitchContainer>
      <button
        className={view === "login" ? "active" : ""}
        onClick={() => changeView("login")}>
        Log in
      </button>
      <button
        className={view === "signup" ? "active" : ""}
        onClick={() => changeView("signup")}>
        Sign up
      </button>
    </SwitchContainer>
  );
};

export default TabSwitch;
