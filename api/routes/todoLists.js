const router = require("express").Router();
const ListModel = require("../models/List");
const multer = require("multer");
const upload = multer();

// get all list data
router.get("/", async (req, res) => {
  try {
    const lists = await ListModel.find({});
    return res.send({ listData: lists });
  } catch (error) {
    return res.send({ error });
  }
});

// add new list
router.post("/add", upload.none(), async (req, res) => {
  const { listName } = req.body;

  try {
    const newList = await new ListModel({ listName: listName, listItems: [] });
    await newList.save();
    const updatedLists = await ListModel.find({});
    return res.send({ listData: updatedLists });
  } catch (error) {
    return res.status(500).send({ error });
  }
});

// add new item to list
router.post("/:listid", upload.none(), async (req, res) => {
  const { listid } = req.params;
  const { newItemName } = req.body;

  try {
    const updatedList = await ListModel.findOneAndUpdate(
      { _id: listid },
      { $push: { listItems: { itemName: newItemName, completed: false } } },
      { new: true }
    );
    return res.send({ listItems: updatedList.listItems });
  } catch (error) {
    return res.status(500).send({ error });
  }
});

// change item completed status
router.put("/:listid/:itemid", upload.none(), async (req, res) => {
  const { listid, itemid } = req.params;
  const { itemCompleted } = req.body;

  try {
    const updatedList = await ListModel.findOneAndUpdate(
      { _id: listid, "listItems._id": itemid },
      { $set: { "listItems.$.completed": itemCompleted } },
      { new: true }
    );
    return res.send({ listItems: updatedList.listItems });
  } catch (error) {
    return res.status(500).send({ error });
  }
});

// delete item
router.patch("/:listid/:itemid", async (req, res) => {
  const { listid, itemid } = req.params;

  try {
    const updatedList = await ListModel.findOneAndUpdate(
      { _id: listid },
      { $pull: { listItems: { _id: { $eq: itemid } } } },
      { new: true }
    );
    return res.send({ listItems: updatedList.listItems });
  } catch (error) {
    return res.status(500).send({ error });
  }
});

// delete list
router.delete("/:listid", async (req, res) => {
  const { listid } = req.params;
  try {
    await ListModel.deleteOne({ _id: listid });
    const updatedLists = await ListModel.find({});
    return res.send({ listData: updatedLists });
  } catch (error) {
    return res.send({ error });
  }
});

module.exports = router;
