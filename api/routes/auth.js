const router = require("express").Router();
const bcrypt = require("bcrypt");
const multer = require("multer");
const upload = multer();
const UserModel = require("../models/User");
const sendEmail = require("../utils/sendEmail");

router.post("/login", upload.none(), async (req, res) => {
  const { email, password } = req.body;
  const existingUser = await UserModel.findOne({ email: email });

  if (!existingUser) {
    return res.status(500).send({ error: "User does not exist. Try again." });
  }

  const passwordIsCorrect = await bcrypt
    .compare(password, existingUser.password)
    .then((isAuth) => isAuth);

  if (!passwordIsCorrect) {
    return res.status(500).send({ error: "Password is incorrect. Try again." });
  }

  // store userid in cookie
  const userId = existingUser._id;
  req.session.userId = userId;

  return res.send({ user: existingUser });
});

router.post("/signup", upload.none(), async (req, res) => {
  const { email, password, confirmPassword, fullName } = req.body;

  if (password !== confirmPassword) {
    return res
      .status(500)
      .send({ error: "Password is not identical.Try again." });
  }

  const existingUser = await UserModel.findOne({ email: email });

  if (existingUser) {
    return res.status(500).send({ error: "Email already exists." });
  }

  const newUser = new UserModel({ email, password, fullName });
  try {
    await newUser.save();
    const mailObject = {
      toEmail: newUser.email,
      subject: "Welcome",
      text: "You successfully signed up. Welcome to the team!",
    };
    await sendEmail(mailObject);
    return res.send({ user: newUser });
  } catch (err) {
    return res.status(500).send({ error: err });
  }
});

router.get("/logout", (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      return res.status(500).send({ error: err });
    }
    res.clearCookie(process.env.SESSION_NAME);
    return res.status(200).send({ data: "Logged out" });
  });
});

router.delete("/:userid", async (req, res) => {
  const { userid } = req.params;

  try {
    const result = await UserModel.deleteOne({ _id: userid });
    if (result.deletedCount > 0) {
      req.session.destroy((error) => {
        if (error) {
          return res.status(500).send({ error });
        }
        res.clearCookie(process.env.SESSION_NAME);
        return res.send({ dCount: result.deletedCount });
      });
    }
  } catch (error) {
    return res.send({ error });
  }
});

module.exports = router;
